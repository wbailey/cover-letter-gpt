from lib import LinkedIn, File, CGPT
from bs4 import BeautifulSoup
import requests
from datetime import datetime
from time import perf_counter, sleep

class CoverLetterWritter:
    def read_urls(self):    
        urls = File("./templates/urls").read().split('\n')
        urls = [url.split(' ') for url in urls]
        urls = [url.strip() for sublist in urls for url in sublist]
        urls = list(filter(lambda url: False if url == "" else True, urls))
        return urls

    def make_cover_letters(self, urls):
        date = str(datetime.now()).replace(' ', '-')
        out_file = File(f"./cover-letters/cover-letters-{date}")
        # read context text 
        context = File("./templates/context")
        # read before text
        before = File("./templates/before")
        # read after text
        after = File("./templates/after")
        cgpt = CGPT(context.read())
        print(f"using {cgpt.model}")
        print()
        written_letters_count = len(urls)
        for i, url in enumerate(urls):
            start = perf_counter()
            print(f"writing cover letter {i + 1} of {len(urls)} using url: {url}")
            # make request(s)
            linked_in = LinkedIn(url)
            should_abort = False
            if not linked_in.description:
                print(f"unable to scrape job title for: {url}")
                should_abort = True
            if not linked_in.title:
                print(f"unable to scrape job description for: {url}")
                should_abort = True
            if should_abort:
                print(f"aborting for posting {i + 1}: {url}")
                print()
                written_letters_count -= 1
                continue
            stop = perf_counter()
            print(f"job posting information retrieved for {linked_in.title} in {stop - start} seconds")

            # have cgpt write cover letter
            posting_prompt = f"{before.read()}\n{linked_in.title}\n{linked_in.description}\n{after.read()}"
            completion = cgpt.prompt(posting_prompt)

            # write to file
            out_file.append(f"\n-----------------------\n")
            out_file.append(f"\nJOB POSTING FOR {linked_in.title}\n")
            out_file.append(f"\n{linked_in.url}\n")
            out_file.append(f"\n{linked_in.description}\n")
            out_file.append(f"\n-----------------------\n")
            out_file.append(f"\nCOVER LETTER FOR {linked_in.title}\n")
            out_file.append(f"\n{completion['choices'][0]['message']['content']}\n")

            stop = perf_counter()
            print(f"cover letter written for {linked_in.title} in {stop - start} seconds")
            print()

        print(f"{written_letters_count} cover letters written to {out_file.file_path}")


    def cli_run(self):
        urls = self.read_urls()
        self.make_cover_letters(urls)
