class File:
    def __init__(self, file_path):
        self.file_path = file_path

    def __str__(self):
        return f"{self.file_path}"

    def read(self):
        with open(self.file_path, "r") as file:
            return file.read()

    def replace(self, new_contents):
        with open(self.file_path, "w") as file:
            file.write(new_contents)    
            return self.read()
    
    def append(self, new_contents):
        with open(self.file_path, "a") as file:
            file.write(new_contents)
            return self.read()


