import openai
from lib import OPENAI_MODEL

class CGPT:
    def __init__(self, context, model = OPENAI_MODEL):
        self.context = context
        self.model = model

    def prompt(self, input_text):
        return openai.ChatCompletion.create(
            model=self.model, 
            messages=[
                {"role": "system", "content": self.context },
                {"role": "user", "content": input_text },

            ]
        )
