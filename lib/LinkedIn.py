import requests
from bs4 import BeautifulSoup

class LinkedIn:
    def __init__(self, url):
        self.url = url
        self.soup = None
        self.description = ""
        self.title = ""

        self.request_soup().parse_soup()

    def __str__(self):
        return f"url: {self.url}, title: {self.title} description: {self.description}"

    def request_soup(self):
        response = requests.get(self.url)
        self.soup = BeautifulSoup(response.text, "html.parser")
        return self

    def parse_soup(self):
        self.description = self.get_text_by_class("div", "show-more-less-html__markup")
        self.title = self.get_text_by_class("h1", "top-card-layout__title")
        return self

    def get_text_by_class(self, element, class_name):
        try:
            return self.soup.find(element, { "class": class_name }).get_text()
        except AttributeError:
            return None

        
