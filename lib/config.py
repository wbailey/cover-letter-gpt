import os
from dotenv import load_dotenv
import openai

load_dotenv()

OPENAI_API_KEY = os.environ.get("OPENAI_API_KEY")
OPENAI_MODEL = os.environ.get("OPENAI_MODEL") if os.environ.get("OPENAI_MODEL") else "gpt-3.5-turbo"
OPENAI_ORG = os.environ.get("OPENAI_ORG")

openai.org = OPENAI_ORG
openai.api_key = OPENAI_API_KEY


