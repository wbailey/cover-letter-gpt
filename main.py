from lib import CoverLetterWritter

def main():
    writer = CoverLetterWritter()
    writer.cli_run()

if __name__ == "__main__":
    main()
